import { AttributeType, BillingMode, Table } from '@aws-cdk/aws-dynamodb';
import { PolicyStatement } from '@aws-cdk/aws-iam';
import { Code, Function, Runtime } from '@aws-cdk/aws-lambda';
import { SqsEventSource } from '@aws-cdk/aws-lambda-event-sources';
import { Queue } from '@aws-cdk/aws-sqs';
import { App, Duration, Stack, StackProps } from '@aws-cdk/core';

export default class SqsMessageDeduplicationStack extends Stack {
  public constructor(scope: App, id: string, props?: StackProps) {
    super(scope, id, props);

    const messageTable = new Table(this,
      'MessageTable',
      {
        tableName: 'message',
        partitionKey: {
          name: 'id',
          type: AttributeType.STRING,
        },
        billingMode: BillingMode.PAY_PER_REQUEST,
      });

    const deadLetterQueue = new Queue(this, 'DeadLetterQueue', {
      queueName: 'message-dead-letter-queue',
    });

    const timeoutSec = Duration.seconds(5);

    const queue = new Queue(this, 'Queue', {
      deadLetterQueue: {
        maxReceiveCount: 3,
        queue: deadLetterQueue,
      },
      queueName: 'message',
      visibilityTimeout: timeoutSec,
    });

    const messageConsumer = new Function(this,
      'MessageConsumer',
      {
        runtime: Runtime.PYTHON_3_7,
        handler: 'src.app.lambda_handler',
        functionName: 'message-consumer',
        environment: {
          'LAMBDA_TIMEOUT': `${timeoutSec}`,
          'MESSAGE_TABLE_NAME': messageTable.tableName,
        },
        description: 'SQS message consumer with DynamoDB deduplication logic.',
        code: Code.asset('.'),
        timeout: timeoutSec,
      });

      messageConsumer.addToRolePolicy(new PolicyStatement({
        actions: ['dynamodb:GetItem', 'dynamodb:PutItem'],
        resources: [messageTable.tableArn],
      }));

      messageConsumer.addEventSource(new SqsEventSource(queue,
        {
          batchSize: 1,
        },
      ));
  }
}
