import json
import logging
from os import getenv
from time import sleep, time

from boto3.dynamodb.conditions import Attr
from botocore.exceptions import ClientError

from src.DynamoDBClient import DynamoDBClient
from src.utils import Key, Status

DYNAMODB_CLIENT: DynamoDBClient = None
LAMBDA_TIMEOUT: int = None
LOGGER = logging.getLogger()

LOGGER.setLevel(logging.INFO)


class ProcessingError(Exception):
    pass


def lambda_handler(event: dict, _context):
    """
    Lambda entry point method processing messages consumed from SQS
    :param event: the event consumed from SQS
    :param _context: the context of the event trigger, which is ignored
    :return: None
    """
    global DYNAMODB_CLIENT
    global LAMBDA_TIMEOUT

    if not DYNAMODB_CLIENT:
        DYNAMODB_CLIENT = setup_dynamodb_client()

    if not LAMBDA_TIMEOUT:
        try:
            LOGGER.info('Fetching the Lambda timeout from environment variable \'LAMBDA_TIMEOUT\'')
            LAMBDA_TIMEOUT = int(getenv('LAMBDA_TIMEOUT'))
        except ValueError as err:
            LOGGER.error(err)
            raise err

    if event and Key.RECORDS.value in event and event[Key.RECORDS.value]:
        LOGGER.info(f'Event triggered containing {len(event[Key.RECORDS.value])} records')
        for record in event[Key.RECORDS.value]:
            message = json.loads(record['body'])
            message_id = message[Key.MESSAGE_ID.value]

            item = query_dynamodb(message_id)

            if item and item[Key.STATUS.value] == Status.COMPLETE.value:
                LOGGER.info(f'Corresponding DynamoDB table item with message ID {message_id} marked COMPLETE.')
                return
            elif not item:
                LOGGER.info(f'Creating a new DynamoDB item with message ID {message_id}')
                item = {Key.MESSAGE_ID.value: message_id, Key.CONSUMPTION_COUNT.value: 0}

            update_dynamodb(item, Status.IN_PROGRESS)

            # replace sleep with your process for SQS messages
            LOGGER.info(f'Waiting')
            sleep(1)

            update_dynamodb(item, Status.COMPLETE)


def setup_dynamodb_client() -> DynamoDBClient:
    """
    Setup a DynamoDB client for the message table
    :return: the DynamoDB client for the message table
    """
    table_name = getenv('MESSAGE_TABLE_NAME', 'message')

    try:
        return DynamoDBClient(table_name, 'id')
    except ClientError as err:
        raise err


def query_dynamodb(message_id: str) -> dict:
    """
    Query the DynamoDB 'message' table for the item containing the given message ID
    :param message_id: the message ID to be included in the query
    :return: the item containing the message ID if it exists, otherwise None
    """
    try:
        response = DYNAMODB_CLIENT.get_item(message_id)
    except ClientError as err:
        raise err

    if Key.ITEM.value in response:
        LOGGER.info(f'Item with ID {message_id} read from DynamoDB table \'{DYNAMODB_CLIENT.table.table_name}\'')
        return response[Key.ITEM.value]


def update_dynamodb(item: dict, status: Status):
    """
    Update the given DynamoDB item's consumption count, status, and updated timestamp
    :param item: the DynamoDB item to be updated, which includes the message ID
    :param status: the new status to assign to the DynamoDB item
    :return: None
    """
    LOGGER.info(f'Incrementing item consumption count and setting item status to {status}')
    item[Key.CONSUMPTION_COUNT.value] += 1
    item[Key.STATUS.value] = status.value
    item[Key.UPDATED.value] = int(time())

    condition_expression = Attr(Key.STATUS.value).eq(Status.IN_PROGRESS.value) & \
                           Attr(Key.UPDATED.value).lt(int(time()) - LAMBDA_TIMEOUT)

    try:
        DYNAMODB_CLIENT.put_item(item, condition_expression)
    except ClientError as err:
        if status == Status.IN_PROGRESS:
            raise ProcessingError(err)
