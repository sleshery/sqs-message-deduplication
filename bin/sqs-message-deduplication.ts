#!/usr/bin/env node
import 'source-map-support/register';
import { App } from '@aws-cdk/core';
import SqsMessageDeduplicationStack from '../lib/sqs-message-deduplication-stack';

const app = new App();
new SqsMessageDeduplicationStack(app, 'SqsMessageDeduplicationStack');
app.synth();
